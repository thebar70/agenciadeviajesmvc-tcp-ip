/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import negocio.GestorClientesPotenciales;

/**
 *
 * @author thebar70
 */
public class GUIClientesPotencialesController implements java.awt.event.ActionListener {

    private GestorClientesPotenciales modelo;
    private GUIClientesPotenciales vista;

    @Override
    public void actionPerformed(ActionEvent e) {    
        modelo.buscarClientesPotenciales(vista.getPaqueteTuristico());
        
    }

    public void addModel(GestorClientesPotenciales m) {
        this.modelo = m;
    }

    public void addView(GUIClientesPotenciales v) {
        this.vista = v;
    }

    

}
