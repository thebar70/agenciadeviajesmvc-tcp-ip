package clienteMain;

import negocio.GestorClientesPotenciales;
import presentacion.GUIClientesPotenciales;
import presentacion.GUIClientesPotencialesController;
import presentacion.GUIEstadisticas;
import presentacion.GUILIstadoClientesPotenciales;



/**
 * Es el pegamento de la aplición
 *
 * @author Libardo, Julio, Ricardo
 */
public class RunMVC {

    public RunMVC() {
        GestorClientesPotenciales myModel = new GestorClientesPotenciales();
        GUIClientesPotenciales myView = new GUIClientesPotenciales();
        
        GUILIstadoClientesPotenciales myView1 = new GUILIstadoClientesPotenciales();
        GUIEstadisticas myView2 = new GUIEstadisticas();
        
        


        myModel.addObserver(myView);
        myModel.addObserver(myView1);
        myModel.addObserver(myView2);
       
        GUIClientesPotencialesController myController = new GUIClientesPotencialesController();
        myController.addModel(myModel);
        myController.addView(myView);
        myView.addController(myController);
        myModel.buscarPaquetesActuales();
        
        

    }
}
