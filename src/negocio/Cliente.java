package negocio;

/**
 *
 * @author Libardo, Ricardo, Julio Representa un cliente de la agencia de viajes
 * con sus atributos
 *
 */
public class Cliente {

    private String cedula;
    private String nombres;
    private String apellidos;
    private String email;
    private String sexo;
    private String edad;

    /**
     * Constructor parametrizado
     *
     * @param id cedula
     * @param nombres los dos nombres
     * @param apellidos los dos apellidos
     * @param direccion direccion donde vive
     * @param celular telefono movil
     * @param email correo electronico
     * @param sexo Género, masculino o femenino
     * @param edad edad del cliente
     */
    public Cliente(String id, String nombres, String apellidos, String email, String sexo, String edad) {
        this.cedula = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
       
        this.email = email;
        this.sexo = sexo;
        this.edad=edad;
    }

    public Cliente() {

    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String id) {
        this.cedula = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

}
