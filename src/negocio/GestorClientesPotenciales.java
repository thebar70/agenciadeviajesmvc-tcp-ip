/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import acceso.IServidorCentral;
import acceso.ServicioServidorCentralSocket;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import com.google.gson.JsonParseException;
import java.util.ArrayList;

import java.util.Properties;

/**
 *
 * @author thebar70
 */
public class GestorClientesPotenciales extends java.util.Observable {

    private final IServidorCentral servidorAgenciaViajes;
    private static Cliente[] clientes;
    private static infoPaquete[] paquetes;
    

    public GestorClientesPotenciales() {
        servidorAgenciaViajes = new ServicioServidorCentralSocket();

    }
    public Cliente[] getClientes(){
        return clientes;
    }
    public infoPaquete[] getPaquetes(){
        return paquetes;
    }
    public void buscarClientesPotenciales(int codigoPaqueteTuristico) {
        //String codigo=String.valueOf(codigoPaqueteTuristico);
        String codigo = String.valueOf(codigoPaqueteTuristico);
        clientes=buscarClientesServidor(codigo);
       
        setChanged();
        notifyObservers();
    }

    /**
     * Busca un cliente en el servidor remoto de la registraduría
     *
     * @param id identificador del clilente
     * @return Objeto tipo Cliente, null si no lo encuentra
     */
    private Cliente[] buscarClientesServidor(String id) {
        //Obtiene el objeto json serializado al servidor de la registraduria
        String json = servidorAgenciaViajes.consultarClientesPotenciales(id);
        if (!json.equals("NO_ENCONTRADO")) {
            //Lo encontró
            return parseToCliente(json);
           
        }
        return null;
    }

    public void buscarPaquetesActuales() {
        String json = servidorAgenciaViajes.consulatPaquetesActuales();
        GestorClientesPotenciales.paquetes = parseToArray(json);
        setChanged();
        notifyObservers();
    }

    /**
     * Deserializa el objeto json y lo convierte en un objeto Cliente
     *
     * @param cliente Objeto tipo Cliente
     * @param json objeto cliente en formato json
     */
    private Cliente[] parseToCliente(String json) {
       
        Cliente[] clientes = new Gson().fromJson(json,Cliente[].class);
        System.out.println("pase");
        return clientes;

    }

    private infoPaquete[] parseToArray(String jsn) {
        //  ArrayList<infoPaquete> paquetes = new ArrayList<infoPaquete>();

        infoPaquete[] array = new Gson().fromJson(jsn,infoPaquete[].class);
        //   for (Object elem : array) {
        //     String paquete = elem.getAsJsonObject().get("nombre").getAsString();
        //paquetes.add(paquete);
        

        return array;
    }

}
