/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author thebar70
 */
public class infoPaquete {
    private String nombre;
    private int cod;
    private String descripcion;
    private String edad_base;
    private String edad_tope;
    private String genero;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEdad_base() {
        return edad_base;
    }

    public void setEdad_base(String edad_base) {
        this.edad_base = edad_base;
    }

    public String getEdad_tope() {
        return edad_tope;
    }

    public void setEdad_tope(String edad_tope) {
        this.edad_tope = edad_tope;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
    
}
